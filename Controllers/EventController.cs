using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using manexp_api.Models;
namespace manexp_api.Controllers
{
    [Route("api/[controller]")]
    public class EventController : Controller
    {
         private readonly XPDBContext _context;

        public EventController(XPDBContext context)
        {
            _context = context;

            if (_context.Event.Count() == 0)
            {
                _context.Event.Add(new Event { EventID=0});
                _context.SaveChanges();
            }
        }
        [HttpGet]
                public IEnumerable<Event> GetAll()
            {
                return _context.Event.ToList();
            }

        [HttpGet("{id}", Name = "GetEvent")]
                public IActionResult GetById(int id)
            {
                var item = _context.Event.FirstOrDefault(t => t.EventID == id);
            if (item == null)
            {
                return NotFound();
            }
                return new ObjectResult(item);
            }

        [HttpPost]
                public IActionResult Create([FromBody] Event item)
{
            if (item == null)
            {
                return BadRequest();
            }

               _context.Event.Add(item);
                _context.SaveChanges(); 

                return CreatedAtRoute("GetEvent", new { id = item.EventID }, item);
}
     [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Event item)
        {
                if (item == null || item.EventID != id)
        {
                return BadRequest();
        }

                var evt = _context.Event.FirstOrDefault(t => t.EventID == id);
                if (evt == null)
        {
                return NotFound();
        }

                evt.EventID = item.EventID;
                evt.Time= item.Time;
                evt.Date= item.Date;
                evt.Location= item.Location;
                evt.Title= item.Title;
                
                
                      
                _context.Event.Update(evt);
                _context.SaveChanges();
                return new NoContentResult();
        }

        [HttpDelete("{id}")]
                public IActionResult Delete(int id)
        {
                var evt = _context.Event.FirstOrDefault(t => t.EventID == id);
                if (evt == null)
        {
                return NotFound();
        }

                _context.Event.Remove(evt);
                _context.SaveChanges();
                return new NoContentResult();         
    }

        
 
}
}


    