using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using manexp_api.Models;
namespace manexp_api.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
          private readonly XPDBContext _context;

        public UserController(XPDBContext context)
        {
            _context = context;

            if (_context.User.Count() == 0)
            {
                _context.User.Add(new User { UserID=0});
                _context.SaveChanges();
            }
        }
        [HttpGet]
                public IEnumerable<User> GetAll()
            {
                return _context.User.ToList();
            }

        [HttpGet("{id}", Name = "GetUser")]
                public IActionResult GetById(int id)
            {
                var item = _context.User.FirstOrDefault(t => t.UserID == id);
            if (item == null)
            {
                return NotFound();
            }
                return new ObjectResult(item);
            }

        [HttpPost]
                public IActionResult Create([FromBody] User item)
{
            if (item == null)
            {
                return BadRequest();
            }

               _context.User.Add(item);
                _context.SaveChanges(); 

                return CreatedAtRoute("GetUser", new { id = item.UserID }, item);
}
    }
}
