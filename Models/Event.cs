using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions;
using System.ComponentModel.DataAnnotations;
using manexp_api.Models;

namespace manexp_api.Models
{
    public class Event{
        public int EventID { get; set; }
        public string Title { get; set; }
        public string Time { get; set; }
        public DateTime Date { get; set; }
        public string Location { get; set; }

       // public List<Checkin> User {get; set;}

    }
}