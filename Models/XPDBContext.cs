using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions;
using manexp_api.Models;

namespace manexp_api.Models
{
    public class XPDBContext : DbContext
    {
        

        public XPDBContext()
        {
        }

        public XPDBContext(DbContextOptions<XPDBContext> options) : base(options)
        {
        }
        // Everything below creates the tables in the database 
                public DbSet<Event> Event { get; set; }
             //   public DbSet<Login> Login { get; set; }
             //   public DbSet<Checkin> Checkin { get; set; }
                public DbSet<User> User{ get; set; }
           protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Event>().ToTable("Event");
          //  modelBuilder.Entity<Login>().ToTable("Login");
          //  modelBuilder.Entity<Checkin>().ToTable("Checkin");
            modelBuilder.Entity<User>().ToTable("User");
            
        }
    }
}